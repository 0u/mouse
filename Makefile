all: dist windows linux

pwd := $(shell pwd)

dist:
	mkdir -p dist
	chown --reference=. ./dist

windows:
	@echo "Building windows binary"
	docker run -v "$(pwd):/src/" cdrx/pyinstaller-windows \
		"pyinstaller -w --clean -y --dist ./dist/windows --workpath /tmp *.spec"
	chown -R --reference=. ./dist/windows

linux:
	@echo "Building linux binary"
	docker run -v "$(pwd):/src/" cdrx/pyinstaller-linux
